from tornado.websocket import WebSocketHandler
from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.httpserver import HTTPServer

connections = []

class EchoWebSocket(WebSocketHandler):
    def open(self):
        connections.append(self)
        print "WebSocket opened"

    def on_message(self, message):
        print message
        self.write_message(u"You said: " + message)

    def on_close(self):
        print "WebSocket closed"

    def check_origin(self, origin):
        return True

def make_app():
    return Application([
        (r"/", EchoWebSocket),
    ])

if __name__ == "__main__":
    
    import thread

    app = make_app()
    
    # run as development app
    app.listen(8888)
    thread.start_new_thread(IOLoop.current().start,tuple())
    while True:
        msg = raw_input('Say anything: ')
        for c in connections:
            c.write_message(msg)
    
    # run as server
    #server = HTTPServer(app)
    #server.bind(8888)
    #server.start(0)